﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // ok as long as this is the only script that loads scenes

public class CollisionHandler : MonoBehaviour {

    [Tooltip("In seconds")][SerializeField] float levelLoadDelay = 1f;
    [Tooltip("FX prefab on player")][SerializeField] GameObject deathFX;

    private void OnTriggerEnter(Collider other)
    {
        StartDeathSequence();

        // blow up
        deathFX.SetActive(true);

        // reload scene
        Invoke("ReloadScene", levelLoadDelay);
    }

    private void StartDeathSequence()
    {
        // disable controls
        SendMessage("OnPlayerDeath");
    }

    private void ReloadScene() // string reference
    {
        SceneManager.LoadScene(1);
    }
}
